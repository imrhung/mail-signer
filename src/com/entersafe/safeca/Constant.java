package com.entersafe.safeca;

public class Constant {
	public static String Debug = "MyDebug";

	//show and close the progress dialog
	public static final int SHOWPROGRESSDIALOG 		= 0x00000010;
	public static final int CLOSEPROGRESSDIALOG 	= 0x00000011;

	public static final int TURNBACK 				= 0x00000000;
	public static final int TOKEN_CONNECT_OK				= 0x00000001;
	public static final int TURNNEXT 				= 0x00000008;
	public static final int SHOWTIME				= 0x00000002;
	public static final int SHOWSIGNTURE			= 0x00000003;
	public static final int LOGINSUCCESS			= 0x00000004;
	public static final int LOGOUT					= 0x00000005;
	public static final int SHOWDIGESTRESULT		= 0x00000006;
	public static final int SHOWSYMENCRESULT		= 0x00000007;
	public static final int SHOWERRINFO				= 0x00000014;
	public static final int CHANGETOUSERINFO		= 0x00000015;
	public static final int VERIFYPASSWORD			= 0x00000100;
	public static final int CHANGEPASSWORD			= 0x00000101;
	public static final int SIGNDATA				= 0x00000200;
	public static final int VERIFYSIGN				= 0x00000201;
	public static final int ENCRYPT					= 0x00000202;
	public static final int DECRYPT					= 0x00000203;
	public static final int VERIFYSIGNPIN			= 0x00000204;
	public static final int DIGEST					= 0x00000300;
	public static final int SYM_DECRYPT				= 0x00000400;
	public static final int SYM_ENCRYPT				= 0x00000401;
	
	
	public static final int BUILD_MAIL_OK 	= 500;
	public static final int SEND_MAIL_EX_OK = 501;
	public static final int SEND_MAIL_OK 	= 502;
	public static final int UP_OFFICE_OK 	= 503;
	public static final int UP_PDF_OK 		= 504;
	public static final int GET_SIGNED_OFFICE_OK 	= 505;
	public static final int GET_SIGNED_PDF_OK 		= 506;
	public static final int SEND_MAIL_EX_FAIL 		= 507;
	public static final int MAIL_SIGNED_PKCS_FAIL 	= 508;
	public static final int MAIL_SIGNED_PKCS_OK 	= 509;
	public static final int BUILD_MAIL_FAIL 		= 510;
	public static final int SERVICE_EXCEPTION 		= 511;
	public static final int SERVICE_COMPLETE 		= 512;
	public static final int GET_PASSWORD 		= 513;
	public static final int WRONG_PASSWORD 		= 515;
	
	public static final int UP_PDF_FAIL 		= 514;
	public static final int GET_SIGNED_PDF_FAIL = 516;
	public static final int PDF_SIGNED_PKCS_OK 	= 517;
	public static final int PDF_SIGNED_PKCS_FAIL= 518;
	
	public static final int UP_OFFICE_FAIL 			= 519;
	public static final int GET_SIGNED_OFFICE_FAIL 	= 520;
	public static final int OFFICE_SIGNED_PKCS_OK 	= 521;
	public static final int OFFICE_SIGNED_PKCS_FAIL	= 522;
	
	public static final int TOKEN_WRONG_PASSWORD	= 523;
	
	public static final int DIALOG_PASSWORD	= 10;
	
	public static final int PICKFILE_RESULT_CODE = 601;
}
