package com.entersafe.safeca;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

public class MyProgressDialog extends ProgressDialog {
	public MyProgressDialog(Context context) {
		super(context);
	}
	
	public MyProgressDialog(Context context, int theme) {
		super(context, theme);
	}
			
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public void setShowInfo(String msg)
	{
		setMessage(msg);
	}
}
