package com.entersafe.safeca;

import android.os.SystemClock;

public class SpendedTime {
	private static long startTime	= 0;
	private static long endTime	= 0;
	
	private static Class<?> timeClass = null;
	
	static boolean setStartTime(Class<?> myclass) {
		if (0 == startTime)
		{
			timeClass = myclass;
			startTime = SystemClock.uptimeMillis();
			return true;
		}
		return false;
	}
	
	static boolean setEndTime(Class<?> myclass) {
		if (0 == endTime)
		{
			if (null != timeClass && timeClass == myclass)
				endTime	= SystemClock.uptimeMillis();
			else
				return false;
			return true;
		}
		return false;
	}
	
	static boolean setTimeZero(Class<?> myclass) {
		if (null != timeClass)
		{
			if (timeClass == myclass)
			{
				timeClass	= null;
				startTime	= 0;
				endTime		= 0;
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}
	
	public static long getTime(Class<?> myclass) {
		long time = endTime - startTime;
		if (null != timeClass && timeClass == myclass)
		{
			setTimeZero(myclass);
		}
		if (time < 0)
			return 0;
		
		return time;
	}
}
