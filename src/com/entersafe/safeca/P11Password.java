package com.entersafe.safeca;


import iaik.pkcs.pkcs11.wrapper.PKCS11;
import iaik.pkcs.pkcs11.wrapper.PKCS11Exception;

public class P11Password {
	public static long changePassword(String oldPassword, String newPassword) throws PKCS11Exception
	{
		// is needs type the OK button, delete set the spend time
		boolean bNeedButtons = true;
		// check password
		if (!bNeedButtons)
		{
			SpendedTime.setStartTime(P11Password.class);
		}
		try {
			if (null == oldPassword || null == newPassword)
				return -1;
			
			P11Other.changePassword(oldPassword, newPassword);
		} catch (PKCS11Exception e) {
			e.printStackTrace();
			return e.getErrorCode();
		}
		
		if (!bNeedButtons)
		{
			SpendedTime.setEndTime(P11Password.class);
		}
		return 0;
	}
	
	static boolean verifityPassword(String password)
	{
		try {
			// is needs type the OK button, delete set the spend time
			boolean bNeedButtons = false;
			// check password
			if (!bNeedButtons)
			{
				SpendedTime.setStartTime(P11Password.class);
			}
			if (null == password)
				return false;
			
			P11Other.loginUser(password);
			if (!bNeedButtons)
			{
				SpendedTime.setEndTime(P11Password.class);
			}
		}
		catch (PKCS11Exception e) {
			e.printStackTrace();
			return false;
		} 
		return true;
	}
    
	public static boolean setTimeZero()
	{
		return SpendedTime.setTimeZero(P11Password.class);
	}
	
	static long getTime()
	{
		return SpendedTime.getTime(P11Password.class);
	}
}
