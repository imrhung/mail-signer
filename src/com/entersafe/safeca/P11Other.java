package com.entersafe.safeca;

import java.io.IOException;


import android.util.Log;


import cfca.mobile.keydevice.AudioToken;
import ft.mobile.bank.utils.Convection;

//import iaik.pkcs.pkcs11.wrapper.CK_INFO;
//import iaik.pkcs.pkcs11.wrapper.CK_SLOT_INFO;
//import iaik.pkcs.pkcs11.wrapper.CK_TOKEN_INFO;

import iaik.pkcs.pkcs11.wrapper.CK_ATTRIBUTE;
import iaik.pkcs.pkcs11.wrapper.CK_MECHANISM;
import iaik.pkcs.pkcs11.wrapper.PKCS11;
import iaik.pkcs.pkcs11.wrapper.PKCS11Connector;
import iaik.pkcs.pkcs11.wrapper.PKCS11Constants;
import iaik.pkcs.pkcs11.wrapper.PKCS11Exception;

public class P11Other {
	public static PKCS11 myPKCS11Module_;
	public static long token_ = -1L;
	public static long session_;
	
	public static AudioToken audioToken;
	public static boolean p11initialize() {
		try {
			System.load("/data/data/com.tomica.mailsigner/lib/libshuttle_p11v220.so");
			System.load("/data/data/com.tomica.mailsigner/lib/libShuttleSDMgr.so");
			
			myPKCS11Module_ = PKCS11Connector.connectToPKCS11Module("/data/data/com.tomica.mailsigner/lib/libshuttle_p11v220.so");
			
			Log.v("Init", "Load library OK");
		} catch (IOException e1) {
			e1.printStackTrace();
			return false;
		}
		
		try {
			myPKCS11Module_.C_Initialize(null);
			Log.v("Init", "C_Initialize OK");
		} catch (PKCS11Exception e) {
			Log.v("Init", "C_Initialize Exeption");
			if (e.getErrorCode() == PKCS11Constants.CKR_CRYPTOKI_ALREADY_INITIALIZED) {
				Log.v("Init", "C_Initialize Connected = true");
				return true;
			}
			e.printStackTrace();
			return false;
		} catch (NullPointerException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void getInfo() throws PKCS11Exception {
		//CK_INFO moduleInfo = myPKCS11Module_.C_GetInfo();
		myPKCS11Module_.C_GetInfo();
	}
	
	public static void getSlotInfo() throws PKCS11Exception {
		long[] slotIDs = myPKCS11Module_.C_GetSlotList(false);
		//CK_SLOT_INFO slotInfo;
		for (int i = 0; i < slotIDs.length; i++) {
			//slotInfo = myPKCS11Module_.C_GetSlotInfo(slotIDs[i]);
			myPKCS11Module_.C_GetSlotInfo(slotIDs[i]);
		}
	}

	public static void getTokenInfo() throws PKCS11Exception {
		long[] tokenIDs = myPKCS11Module_.C_GetSlotList(true);
		//CK_TOKEN_INFO tokenInfo;
		for (int i = 0; i < tokenIDs.length; i++) {
			//tokenInfo = myPKCS11Module_.C_GetTokenInfo(tokenIDs[i]);
			myPKCS11Module_.C_GetTokenInfo(tokenIDs[i]);
			if (token_ == -1L) {
				token_ = tokenIDs[i];
			}
		}
	}

	public static void openSession() throws PKCS11Exception {
		session_ = myPKCS11Module_.C_OpenSession(token_,PKCS11Constants.CKF_SERIAL_SESSION
						| PKCS11Constants.CKF_RW_SESSION, null, null);
	}

	public static void loginUser(String password) throws PKCS11Exception {
		try {
			if (null == password)
				return;
			
			myPKCS11Module_.C_Login(session_, PKCS11Constants.CKU_USER,
					password.toCharArray());
		} catch (PKCS11Exception e) {
			if (e.getErrorCode() == PKCS11Constants.CKR_USER_ALREADY_LOGGED_IN) {
				logout();
				loginUser(password);
			} else if (e.getErrorCode() == PKCS11Constants.CKR_PIN_INCORRECT) {
				throw e;
			} else {
				throw e;
			}
		}
	}
	
	public static void changePassword(String oldPassword, String newPassword) throws PKCS11Exception {
		if (null != oldPassword && null != newPassword)
		{
			try {
				if (null == oldPassword || null == newPassword)
					return;
				
				myPKCS11Module_.C_SetPIN(session_, oldPassword.toCharArray(), newPassword.toCharArray());
			} catch (PKCS11Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
	}
	
    public static long[] findCertificates() throws PKCS11Exception
    {
		long certKeyHandle = -1L;
		
		Log.e(Constant.Debug,"finding all certificates on token ...");
		
		CK_ATTRIBUTE[] attributeTemplateList = new CK_ATTRIBUTE[1];
		
		attributeTemplateList[0] = new CK_ATTRIBUTE();
		attributeTemplateList[0].type = PKCS11Constants.CKA_CLASS;
		attributeTemplateList[0].pValue = new Long(PKCS11Constants.CKO_CERTIFICATE);
		
		myPKCS11Module_.C_FindObjectsInit(session_, attributeTemplateList);
		
		long[] availableCertificates = myPKCS11Module_.C_FindObjects(session_,
		        100);
		//maximum of 100 at once
		
		myPKCS11Module_.C_FindObjectsFinal(session_);
		
		if (availableCertificates == null) {
			Log.e(Constant.Debug,"null returned - no certificate key found");
		} else {
		    Log.e(Constant.Debug, "found " + availableCertificates.length
		                + " certificates");
		}
		
		return availableCertificates;
	}
    
    public static byte[] getDEREncodedCertificate(long certHandle) throws
    PKCS11Exception {
	
    	Log.e(Constant.Debug,"reading certificate bytes");
		byte[] certBytes = null;
		
		CK_ATTRIBUTE[] template = new CK_ATTRIBUTE[1];
		template[0] = new CK_ATTRIBUTE();
		template[0].type = PKCS11Constants.CKA_VALUE;
		
		myPKCS11Module_.C_GetAttributeValue(session_, certHandle, template);
		
		certBytes = (byte[]) template[0].pValue;
		
		return certBytes;
	}
	
	public static long[] findKeypair(int bitSize) throws PKCS11Exception {
		
		CK_ATTRIBUTE[] findPubKeyAttrList = new CK_ATTRIBUTE[2];
		findPubKeyAttrList[0] = new CK_ATTRIBUTE();
		findPubKeyAttrList[0].type = PKCS11Constants.CKA_CLASS;
		findPubKeyAttrList[0].pValue = new Long(PKCS11Constants.CKO_PUBLIC_KEY);
		findPubKeyAttrList[1] = new CK_ATTRIBUTE();
		findPubKeyAttrList[1].type = PKCS11Constants.CKA_MODULUS_BITS;
		findPubKeyAttrList[1].pValue = new Long(bitSize);
		
		myPKCS11Module_.C_FindObjectsInit(session_, findPubKeyAttrList);
		long[] pubKeyHandle = myPKCS11Module_.C_FindObjects(session_, 1);
		myPKCS11Module_.C_FindObjectsFinal(session_);
		
		if(pubKeyHandle == null)
		{
			return null;
		}
		
		int num = pubKeyHandle.length;
		if(num <= 0)
		{
			return null;
		}
		
		long publicKeyHandle_ = pubKeyHandle[0];
		
		CK_ATTRIBUTE[] getModulusAttrList = new CK_ATTRIBUTE[1];
		getModulusAttrList[0] = new CK_ATTRIBUTE();
		getModulusAttrList[0].type = PKCS11Constants.CKA_MODULUS;
		getModulusAttrList[0].pValue = new byte[256];
		
		myPKCS11Module_.C_GetAttributeValue(session_, publicKeyHandle_, getModulusAttrList);
		
		CK_ATTRIBUTE[] priAttrTmplList = new CK_ATTRIBUTE[3];
		
		priAttrTmplList[0] = new CK_ATTRIBUTE();
		priAttrTmplList[0].type = PKCS11Constants.CKA_CLASS;
		priAttrTmplList[0].pValue = new Long(PKCS11Constants.CKO_PRIVATE_KEY);
		
		priAttrTmplList[1] = new CK_ATTRIBUTE();
		priAttrTmplList[1].type = PKCS11Constants.CKA_TOKEN;
		priAttrTmplList[1].pValue = new Boolean(PKCS11Constants.TRUE);
		
		priAttrTmplList[2] = new CK_ATTRIBUTE();
		priAttrTmplList[2].type = PKCS11Constants.CKA_MODULUS;
		priAttrTmplList[2].pValue = getModulusAttrList[0].pValue;
		
		myPKCS11Module_.C_FindObjectsInit(session_, priAttrTmplList);
		long[] priKeyHandle = myPKCS11Module_.C_FindObjects(session_, 1);
		myPKCS11Module_.C_FindObjectsFinal(session_);
		
		if(priKeyHandle == null	|| priKeyHandle.length == 0)
		{
			return null;
		}
		
		long[] keyPair = new long[2];
		keyPair[0] = pubKeyHandle[0];
		keyPair[1] = priKeyHandle[0];
		
		return keyPair;
	}
	
	public static long[] generateKeypair(int bitSize) throws PKCS11Exception {
		//myTextView.append("\n\n\nGENERATING KEYPAIR...\n");
		CK_ATTRIBUTE[] pukAttrTmplList = new CK_ATTRIBUTE[7];
		pukAttrTmplList[0] = new CK_ATTRIBUTE();
		pukAttrTmplList[0].type = PKCS11Constants.CKA_CLASS;
		pukAttrTmplList[0].pValue = new Long(PKCS11Constants.CKO_PUBLIC_KEY);
		pukAttrTmplList[1] = new CK_ATTRIBUTE();
		pukAttrTmplList[1].type = PKCS11Constants.CKA_KEY_TYPE;
		pukAttrTmplList[1].pValue = new Long(PKCS11Constants.CKK_RSA);
		pukAttrTmplList[2] = new CK_ATTRIBUTE();
		pukAttrTmplList[2].type = PKCS11Constants.CKA_SUBJECT;
		byte[] subject = "Sample RSA Key Pair".getBytes();
		pukAttrTmplList[2].pValue = subject;
		pukAttrTmplList[3] = new CK_ATTRIBUTE();
		pukAttrTmplList[3].type = PKCS11Constants.CKA_MODULUS_BITS;
		pukAttrTmplList[3].pValue = new Long(bitSize);
		pukAttrTmplList[4] = new CK_ATTRIBUTE();
		pukAttrTmplList[4].type = PKCS11Constants.CKA_ENCRYPT;
		pukAttrTmplList[4].pValue = new Boolean(PKCS11Constants.TRUE);
		pukAttrTmplList[5] = new CK_ATTRIBUTE();
		pukAttrTmplList[5].type = PKCS11Constants.CKA_TOKEN;
		pukAttrTmplList[5].pValue = new Boolean(PKCS11Constants.TRUE);
		pukAttrTmplList[6] = new CK_ATTRIBUTE();
		pukAttrTmplList[6].type = PKCS11Constants.CKA_WRAP;
		pukAttrTmplList[6].pValue = new Boolean(PKCS11Constants.TRUE);

		CK_ATTRIBUTE[] prkAttrTmplList = new CK_ATTRIBUTE[9];
		prkAttrTmplList[0] = new CK_ATTRIBUTE();
		prkAttrTmplList[0].type = PKCS11Constants.CKA_CLASS;
		prkAttrTmplList[0].pValue = new Long(PKCS11Constants.CKO_PRIVATE_KEY);
		prkAttrTmplList[1] = new CK_ATTRIBUTE();
		prkAttrTmplList[1].type = PKCS11Constants.CKA_KEY_TYPE;
		prkAttrTmplList[1].pValue = new Long(PKCS11Constants.CKK_RSA);
		prkAttrTmplList[2] = new CK_ATTRIBUTE();
		prkAttrTmplList[2].type = PKCS11Constants.CKA_SUBJECT;
		prkAttrTmplList[2].pValue = subject;
		prkAttrTmplList[3] = new CK_ATTRIBUTE();
		prkAttrTmplList[3].type = PKCS11Constants.CKA_DECRYPT;
		prkAttrTmplList[3].pValue = new Boolean(PKCS11Constants.TRUE);
		prkAttrTmplList[4] = new CK_ATTRIBUTE();
		prkAttrTmplList[4].type = PKCS11Constants.CKA_PRIVATE;
		prkAttrTmplList[4].pValue = new Boolean(PKCS11Constants.TRUE);
		prkAttrTmplList[5] = new CK_ATTRIBUTE();
		prkAttrTmplList[5].type = PKCS11Constants.CKA_SENSITIVE;
		prkAttrTmplList[5].pValue = new Boolean(PKCS11Constants.TRUE);
		prkAttrTmplList[6] = new CK_ATTRIBUTE();
		prkAttrTmplList[6].type = PKCS11Constants.CKA_TOKEN;
		prkAttrTmplList[6].pValue = new Boolean(PKCS11Constants.TRUE);
		prkAttrTmplList[7] = new CK_ATTRIBUTE();
		prkAttrTmplList[7].type = PKCS11Constants.CKA_EXTRACTABLE;
		prkAttrTmplList[7].pValue = new Boolean(PKCS11Constants.TRUE);
		prkAttrTmplList[8] = new CK_ATTRIBUTE();
		prkAttrTmplList[8].type = PKCS11Constants.CKA_UNWRAP;
		prkAttrTmplList[8].pValue = new Boolean(PKCS11Constants.TRUE);

		CK_MECHANISM genkeyMechanism_ = new CK_MECHANISM();
		genkeyMechanism_.mechanism = PKCS11Constants.CKM_RSA_PKCS_KEY_PAIR_GEN;
		genkeyMechanism_.pParameter = null;
		long[] objectHandles = myPKCS11Module_.C_GenerateKeyPair(session_,
				genkeyMechanism_, pukAttrTmplList, prkAttrTmplList);
		//publicKeyHandle_ = objectHandles[0];
		//privateKeyHandle_ = objectHandles[1];
		return objectHandles;
		//myTextView.append("public key handle:\n"
		//		+ Long.toString(publicKeyHandle_) + "\n");
		//myTextView.append("private key handle:\n"
		//		+ Long.toString(privateKeyHandle_) + "\n");
		//myTextView.append("FINISHED\n");
	}
	
	public static byte[] signData(byte[] signData, long privateKeyHandle_, long mechanism) throws PKCS11Exception {
		//myTextView.append("\n\n\nSIGNING DATA...\n");
		if (null == signData)
			return null;
		
		//########################
		//signData = "110000000284210000000200<?xml version=\"1.0\" encoding=\"GBK\" ?><TradeData><field name=\"支付卡（账）号\" value=\"9558880200005531792\" DisplayOnScreen=\"TRUE\"/><field name=\"转账金额\" value=\"100\" DisplayOnScreen=\"TRUE\"/></TradeData>220000000048支付卡（账）号:9558880200005531792转账金额:100230000000000";
		//String strSignData = "110000001045210000000965<?xml version=\"1.0\" encoding=\"UTF-8\"?><TradeData><field name=\"对账单类型\" value=\"指定账户对账单\" DisplayOnScreen=\"TRUE\"/><field name=\"对账频度\" value=\"按日\" DisplayOnScreen=\"TRUE\"/><field name=\"对账周期\" value=\"1天\" DisplayOnScreen=\"TRUE\"/><field name=\"首次对账日\" value=\"2016-07-31\" DisplayOnScreen=\"TRUE\"/><field name=\"对账期数\" value=\"1\" DisplayOnScreen=\"TRUE\"/><field name=\"指定折算币种\" value=\"美元\" DisplayOnScreen=\"TRUE\"/><field name=\"是否自动展期\" value=\"是\" DisplayOnScreen=\"TRUE\"/><field name=\"展期期数\" value=\"1天\" DisplayOnScreen=\"TRUE\"/><field name=\"参与对账账户\" value=\"1.  账号:0119000100001025238\" DisplayOnScreen=\"TRUE\"/><field name=\"接收方式\" value=\"(1). 自取 自取网点:胡博\" DisplayOnScreen=\"TRUE\"/><field name=\"登录ID\" value=\"0119000100001025238\" DisplayOnScreen=\"TRUE\"/><field name=\"交易提交时间\" value=\"2016-07-30 11:44:06\" DisplayOnScreen=\"TRUE\"/></TradeData>220000000044请确认签订对账单协议(工商银行)230000000000";
		//String strSignData = "110000001045210000000965<?xml version=\"1.0\" encoding=\"UTF-8\"?><TradeData><field name=\"对账单类型\" value=\"指定账户对账单\" DisplayOnScreen=\"TRUE\"/><field name=\"对账频度\" value=\"按日\" DisplayOnScreen=\"TRUE\"/><field name=\"对账周期\" value=\"1天\" DisplayOnScreen=\"TRUE\"/><field name=\"首次对账日\" value=\"2016-07-31\" DisplayOnScreen=\"TRUE\"/><field name=\"对账期数\" value=\"1\" DisplayOnScreen=\"TRUE\"/><field name=\"指定折算币种\" value=\"美元\" DisplayOnScreen=\"TRUE\"/><field name=\"是否自动展期\" value=\"是\" DisplayOnScreen=\"TRUE\"/><field name=\"展期期数\" value=\"1天\" DisplayOnScreen=\"TRUE\"/><field name=\"参与对账账户\" value=\"\r\n1.  账号:0119000100001025238\" DisplayOnScreen=\"TRUE\"/><field name=\"接收方式\" value=\"\r\n(1). 自取 自取网点:胡博\" DisplayOnScreen=\"TRUE\"/><field name=\"登录ID\" value=\"0119000100001025238\" DisplayOnScreen=\"TRUE\"/><field name=\"交易提交时间\" value=\"2016-07-30 11:44:06\" DisplayOnScreen=\"TRUE\"/></TradeData>220000000044请确认签订对账单协议(工商银行)230000000000";
		//String strSignData = "110000000897210000000810<?xml version=\"1.0\" encoding=\"UTF-8\"?><TradeData><field name=\"金额\" value=\"123.00元\" DisplayOnScreen=\"TRUE\"/><field name=\"大写金额\" value=\"壹佰贰拾叁元整\" DisplayOnScreen=\"TRUE\"/><field name=\"汇款单位名称\" value=\"北京捌零后广告制作有限公司\" DisplayOnScreen=\"TRUE\"/><field name=\"汇款单位账号\" value=\"0200205309200001231\" DisplayOnScreen=\"TRUE\"/><field name=\"汇款单位开户行名称\" value=\"和平街支行\" DisplayOnScreen=\"TRUE\"/><field name=\"收款单位名称\" value=\"多来提·穆海麦提\" DisplayOnScreen=\"TRUE\"/><field name=\"收款单位账号\" value=\"6222081208000335070\" DisplayOnScreen=\"TRUE\"/><field name=\"收款单位开户行名称\" value=\"icbc\" DisplayOnScreen=\"TRUE\"/><field name=\"登陆ID\" value=\"80hou0.c.0200\" DisplayOnScreen=\"TRUE\"/><field name=\"交易提交时间\" value=\"2010050115061937529489315\" DisplayOnScreen=\"TRUE\"/></TradeData>220000000051金额:123.00元RMB 账号:6222081208000335070(工商银行)230000000000";				
		
		//#########################
	
		//byte[] data = signData.getBytes();
		/*
		byte[] data = null;
		try {
			data = signData.getBytes("GBK");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		//byte[] data = Convection.getUtf8Bytes(strSignData);
		//int dataLen = data.length;
		//byte[] data = signData.getBytes();
		
		
		CK_MECHANISM signatureMechanism_ = new CK_MECHANISM();
		signatureMechanism_.mechanism = mechanism;
		signatureMechanism_.pParameter = null;

		myPKCS11Module_.C_SignInit(session_, signatureMechanism_,
				privateKeyHandle_);
		
		byte[] signature_ = myPKCS11Module_.C_Sign(session_, signData);
		
		//myTextView.append("signature: \n" + byteArrayToString(signature_)
		//		+ "\nlen:" + Integer.toString(signature_.length) + "\n");
		//myTextView.append("FINISHED\n");
		return signature_;
	}

	public static void verifySignature(byte[] signData, byte[] signature_, long publicKeyHandle_, long mechanism) throws PKCS11Exception {
		//myTextView.append("\n\n\nVERIFYING SIGNATURE...\n");
		if (null == signData || null == signature_)
			return;
		
		//String strSignData = "110000001045210000000965<?xml version=\"1.0\" encoding=\"UTF-8\"?><TradeData><field name=\"对账单类型\" value=\"指定账户对账单\" DisplayOnScreen=\"TRUE\"/><field name=\"对账频度\" value=\"按日\" DisplayOnScreen=\"TRUE\"/><field name=\"对账周期\" value=\"1天\" DisplayOnScreen=\"TRUE\"/><field name=\"首次对账日\" value=\"2016-07-31\" DisplayOnScreen=\"TRUE\"/><field name=\"对账期数\" value=\"1\" DisplayOnScreen=\"TRUE\"/><field name=\"指定折算币种\" value=\"美元\" DisplayOnScreen=\"TRUE\"/><field name=\"是否自动展期\" value=\"是\" DisplayOnScreen=\"TRUE\"/><field name=\"展期期数\" value=\"1天\" DisplayOnScreen=\"TRUE\"/><field name=\"参与对账账户\" value=\"\r\n1.  账号:0119000100001025238\" DisplayOnScreen=\"TRUE\"/><field name=\"接收方式\" value=\"\r\n(1). 自取 自取网点:胡博\" DisplayOnScreen=\"TRUE\"/><field name=\"登录ID\" value=\"0119000100001025238\" DisplayOnScreen=\"TRUE\"/><field name=\"交易提交时间\" value=\"2016-07-30 11:44:06\" DisplayOnScreen=\"TRUE\"/></TradeData>220000000044请确认签订对账单协议(工商银行)230000000000";				
		//byte[] data = Convection.getUtf8Bytes(strSignData);
		//int dataLen = data.length;
		
		CK_MECHANISM signatureMechanism_ = new CK_MECHANISM();
		signatureMechanism_.mechanism = mechanism;
		signatureMechanism_.pParameter = null;
		try {
			myPKCS11Module_.C_VerifyInit(session_, signatureMechanism_,
					publicKeyHandle_);
			myPKCS11Module_.C_Verify(session_, signData, signature_);
		} catch (PKCS11Exception e) {
			if (e.getErrorCode() != PKCS11Constants.CKR_OK) {
				//myTextView.append("verification failed.\n");
				//myTextView.append("error code: " + e.getMessage() + "\n");
			}
			e.printStackTrace();
			throw e;
		}
		//myTextView.append("verification succeed.\n");
		//myTextView.append("FINISHED\n");
	}
	
	public static byte[] symEncryptData(String strKey, String strData, long algorithm) throws PKCS11Exception 
	{	
		long hSymKey;
		long keyType = 0; 
		long keyLen = 0;

		if (null == strKey || null == strData)
			return null;
		
		if(algorithm == PKCS11Constants.CKM_DES_ECB
			|| algorithm == PKCS11Constants.CKM_DES_CBC)
		{
			keyType = PKCS11Constants.CKK_DES;
			keyLen = 8;
		}
		else if(algorithm == PKCS11Constants.CKM_DES3_ECB
			|| algorithm == PKCS11Constants.CKM_DES3_CBC)
		{
			//keyType = PKCS11Constants.CKK_DES3;
			keyType = PKCS11Constants.CKK_DES3;
			keyLen = 24;
		}
		
		byte[] symkey = Convection.hexString2Bytes(strKey);
		byte[] data = Convection.hexString2Bytes(strData);
				
		CK_ATTRIBUTE[] symKeyTemplateList = new CK_ATTRIBUTE[8];
		symKeyTemplateList[0] = new CK_ATTRIBUTE();
		symKeyTemplateList[0].type = PKCS11Constants.CKA_CLASS;
		symKeyTemplateList[0].pValue = new Long(PKCS11Constants.CKO_SECRET_KEY);
		symKeyTemplateList[1] = new CK_ATTRIBUTE();
		symKeyTemplateList[1].type = PKCS11Constants.CKA_KEY_TYPE;
		symKeyTemplateList[1].pValue = new Long(keyType);
		symKeyTemplateList[2] = new CK_ATTRIBUTE();
		symKeyTemplateList[2].type = PKCS11Constants.CKA_TOKEN;
		symKeyTemplateList[2].pValue = new Boolean(PKCS11Constants.FALSE);
		symKeyTemplateList[3] = new CK_ATTRIBUTE();
		symKeyTemplateList[3].type = PKCS11Constants.CKA_PRIVATE;
		symKeyTemplateList[3].pValue = new Boolean(PKCS11Constants.FALSE);
		symKeyTemplateList[4] = new CK_ATTRIBUTE();
		symKeyTemplateList[4].type = PKCS11Constants.CKA_ENCRYPT;
		symKeyTemplateList[4].pValue = new Boolean(PKCS11Constants.TRUE);
		symKeyTemplateList[5] = new CK_ATTRIBUTE();
		symKeyTemplateList[5].type = PKCS11Constants.CKA_DECRYPT;
		symKeyTemplateList[5].pValue = new Boolean(PKCS11Constants.TRUE);
		symKeyTemplateList[6] = new CK_ATTRIBUTE();
		symKeyTemplateList[6].type = PKCS11Constants.CKA_VALUE;
		symKeyTemplateList[6].pValue = symkey;//??????????????????????
		symKeyTemplateList[7] = new CK_ATTRIBUTE();
		symKeyTemplateList[7].type = PKCS11Constants.CKA_VALUE_LEN;
		symKeyTemplateList[7].pValue = new Long(keyLen);
		
	    byte iv[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		CK_MECHANISM ckMechanism = new CK_MECHANISM();
		ckMechanism.mechanism = algorithm;
		ckMechanism.pParameter = iv;
		
		try {
			hSymKey = myPKCS11Module_.C_CreateObject(session_, symKeyTemplateList);
		} catch (PKCS11Exception e) {
			if (e.getErrorCode() != PKCS11Constants.CKR_OK) {
				//myTextView.append("encryption failed.\n");
				//myTextView.append("error code: " + e.getMessage() + "\n");
			}
			e.printStackTrace();
			throw e;
		}
		
		byte[] cipher_ = null;
		try {
			myPKCS11Module_.C_EncryptInit(session_, ckMechanism,
					hSymKey);
			cipher_ = myPKCS11Module_.C_Encrypt(session_, data);
		} catch (PKCS11Exception e) {
			// TODO Auto-generated catch block
			if (e.getErrorCode() != PKCS11Constants.CKR_OK) {
			}
			e.printStackTrace();
		}		
		
		try {
			myPKCS11Module_.C_DestroyObject(session_, hSymKey);
		} catch (PKCS11Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cipher_;		
	}
	
	public static byte[] symDecryptData(String strKey, String strData, long algorithm) throws PKCS11Exception 
	{	
		long hSymKey;
		long keyType = 0; 
		long keyLen = 0;
		if (null == strKey || null == strData)
			return null;
			
		if(algorithm == PKCS11Constants.CKM_DES_ECB
			|| algorithm == PKCS11Constants.CKM_DES_CBC)
		{
			keyType = PKCS11Constants.CKK_DES;
			keyLen = 8;
		}
		else if(algorithm == PKCS11Constants.CKM_DES3_ECB
			|| algorithm == PKCS11Constants.CKM_DES3_CBC)
		{
			//keyType = PKCS11Constants.CKK_DES3;
			keyType = PKCS11Constants.CKK_DES3;
			keyLen = 24;
		}
		
		byte[] symkey = Convection.hexString2Bytes(strKey);
		byte[] data = Convection.hexString2Bytes(strData);
				
		CK_ATTRIBUTE[] symKeyTemplateList = new CK_ATTRIBUTE[8];
		symKeyTemplateList[0] = new CK_ATTRIBUTE();
		symKeyTemplateList[0].type = PKCS11Constants.CKA_CLASS;
		symKeyTemplateList[0].pValue = new Long(PKCS11Constants.CKO_SECRET_KEY);
		symKeyTemplateList[1] = new CK_ATTRIBUTE();
		symKeyTemplateList[1].type = PKCS11Constants.CKA_KEY_TYPE;
		symKeyTemplateList[1].pValue = new Long(keyType);
		symKeyTemplateList[2] = new CK_ATTRIBUTE();
		symKeyTemplateList[2].type = PKCS11Constants.CKA_TOKEN;
		symKeyTemplateList[2].pValue = new Boolean(PKCS11Constants.FALSE);
		symKeyTemplateList[3] = new CK_ATTRIBUTE();
		symKeyTemplateList[3].type = PKCS11Constants.CKA_PRIVATE;
		symKeyTemplateList[3].pValue = new Boolean(PKCS11Constants.FALSE);
		symKeyTemplateList[4] = new CK_ATTRIBUTE();
		symKeyTemplateList[4].type = PKCS11Constants.CKA_ENCRYPT;
		symKeyTemplateList[4].pValue = new Boolean(PKCS11Constants.TRUE);
		symKeyTemplateList[5] = new CK_ATTRIBUTE();
		symKeyTemplateList[5].type = PKCS11Constants.CKA_DECRYPT;
		symKeyTemplateList[5].pValue = new Boolean(PKCS11Constants.TRUE);
		symKeyTemplateList[6] = new CK_ATTRIBUTE();
		symKeyTemplateList[6].type = PKCS11Constants.CKA_VALUE;
		symKeyTemplateList[6].pValue = symkey;//??????????????????????
		symKeyTemplateList[7] = new CK_ATTRIBUTE();
		symKeyTemplateList[7].type = PKCS11Constants.CKA_VALUE_LEN;
		symKeyTemplateList[7].pValue = new Long(keyLen);
		
	    byte iv[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
		CK_MECHANISM ckMechanism = new CK_MECHANISM();
		ckMechanism.mechanism = algorithm;
		ckMechanism.pParameter = iv;
		
		try {
			hSymKey = myPKCS11Module_.C_CreateObject(session_, symKeyTemplateList);
		} catch (PKCS11Exception e) {
			if (e.getErrorCode() != PKCS11Constants.CKR_OK) {
				//myTextView.append("encryption failed.\n");
				//myTextView.append("error code: " + e.getMessage() + "\n");
			}
			e.printStackTrace();
			throw e;
		}
		
		byte[] cipher_ = null;
		try {
			myPKCS11Module_.C_DecryptInit(session_, ckMechanism,
					hSymKey);
			cipher_ = myPKCS11Module_.C_Decrypt(session_, data);
		} catch (PKCS11Exception e) {
			// TODO Auto-generated catch block
			if (e.getErrorCode() != PKCS11Constants.CKR_OK) {
			}
			e.printStackTrace();
		}		
		
		try {
			myPKCS11Module_.C_DestroyObject(session_, hSymKey);
		} catch (PKCS11Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cipher_;		
	}
	
	public static byte[] encryptData(String encryptdata, long publicKeyHandle_) throws PKCS11Exception {
		//myTextView.append("\n\n\nENCRYPTING DATA...\n");

		if (null == encryptdata)
			return null;
		
		byte[] data = encryptdata.getBytes();
		//myTextView.append("orignal: \n" + byteArrayToString(data) + "\nlen:"
		//		+ Integer.toString(data.length) + "\n");
		CK_MECHANISM encryptMechanism_ = new CK_MECHANISM();
		encryptMechanism_.mechanism = PKCS11Constants.CKM_RSA_PKCS;
		encryptMechanism_.pParameter = null;
		byte[] cipher_;
		try {
			myPKCS11Module_.C_EncryptInit(session_, encryptMechanism_,
					publicKeyHandle_);
			cipher_ = myPKCS11Module_.C_Encrypt(session_, data);
		} catch (PKCS11Exception e) {
			if (e.getErrorCode() != PKCS11Constants.CKR_OK) {
				//myTextView.append("encryption failed.\n");
				//myTextView.append("error code: " + e.getMessage() + "\n");
			}
			e.printStackTrace();
			throw e;
		}
		//myTextView.append("cipher: \n" + byteArrayToString(cipher_) + "\nlen:"
		//		+ Integer.toString(cipher_.length) + "\n");
		//myTextView.append("FINISHED\n");
		return cipher_;
	}
	
	public static byte[] decryptData(byte[] cipher_, long privateKeyHandle_) throws PKCS11Exception {
		//myTextView.append("\n\n\nDERYPTING DATA...\n");
		byte[] plain = null;
		if (null == cipher_)
			return null;
		
		CK_MECHANISM encryptMechanism_ = new CK_MECHANISM();
		encryptMechanism_.mechanism = PKCS11Constants.CKM_RSA_PKCS;
		encryptMechanism_.pParameter = null;
		try {
			myPKCS11Module_.C_DecryptInit(session_, encryptMechanism_,
					privateKeyHandle_);
			plain = myPKCS11Module_.C_Decrypt(session_, cipher_);
		} catch (PKCS11Exception e) {
			//myTextView.append("decryption failed.\n");
			//myTextView.append("error code: " + e.getMessage() + "\n");
			e.printStackTrace();
			throw e;
		}
		//myTextView.append("plain: \n" + byteArrayToString(plain) + "\nlen:"
		//		+ Integer.toString(plain.length) + "\n");
		//myTextView.append("FINISHED\n");
		return plain;
	}
	
	public static byte[] digest(String data, long mechanism) throws PKCS11Exception {
		byte[] digest = null;

		if (null == data)
			return null;
		
		CK_MECHANISM Mechanism_ = new CK_MECHANISM();
		Mechanism_.mechanism = mechanism;
		Mechanism_.pParameter = null;
		try {
			myPKCS11Module_.C_DigestInit(session_, Mechanism_);
			digest = myPKCS11Module_.C_Digest(session_, data.getBytes());
		} catch (PKCS11Exception e) {
			e.printStackTrace();
			throw e;
		}
		return digest;
	}
	
	public static byte[] digest(byte[] data, long mechanism) throws PKCS11Exception {
		byte[] digest = null;

		if (null == data)
			return null;
		
		CK_MECHANISM Mechanism_ = new CK_MECHANISM();
		Mechanism_.mechanism = mechanism;
		Mechanism_.pParameter = null;
		try {
			myPKCS11Module_.C_DigestInit(session_, Mechanism_);
			digest = myPKCS11Module_.C_Digest(session_, data);
		} catch (PKCS11Exception e) {
			e.printStackTrace();
			throw e;
		}
		return digest;
	}
	
	public static void cleanup(long[] objectHandles) throws PKCS11Exception {
		//myTextView.append("\n\n\nCLEAN UP...\n");
		
		if (null == objectHandles || objectHandles.length < 2)
			return;
		
		try {
			myPKCS11Module_.C_DestroyObject(session_, objectHandles[0]);
			myPKCS11Module_.C_DestroyObject(session_, objectHandles[1]);
		} catch (PKCS11Exception e) {
			//myTextView.append("cleaning up failed.\n");
			//myTextView.append("error code: " + e.getMessage() + "\n");
			e.printStackTrace();
			throw e;
		}
		
		//myTextView.append("FINISHED\n");
	}

	public static void logout() throws PKCS11Exception {
		//myTextView.append("\n\n\nLOGGING OUT SESSION...\n");
		myPKCS11Module_.C_Logout(session_);
		//myTextView.append("FINISHED\n");
	}
	
	public static void p11finalize() {
		try {
			myPKCS11Module_.C_Finalize(null);
			myPKCS11Module_.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
