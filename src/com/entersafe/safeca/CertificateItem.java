package com.entersafe.safeca;

import java.math.BigInteger;
import java.security.Principal;
import java.util.Date;

public class CertificateItem extends Object
{
	Principal issuerInfo;
	Principal subjectInfo;
	Date dateValid;
	Date dateExpire;
	BigInteger serialNumber;
	String AlgName;
	int version;
	String SigAlgOID;
	
	public CertificateItem(Principal issuerInfo, Principal subjectInfo, Date dateValid, Date dateExpire
			, BigInteger serialNumber, String AlgName, int version, String SigAlgOID)
	{
		this.issuerInfo 	= issuerInfo;
		this.subjectInfo 	= subjectInfo;
		this.dateValid 		= dateValid;
		this.dateExpire 	= dateExpire;
		this.serialNumber 	= serialNumber;
		this.AlgName 		= AlgName;
		this.version 		= version;
		this.SigAlgOID 		= SigAlgOID;
	}
}
