package com.entersafe.utils;

/*
 * Get battery status, returns 0 indicate the capacity of the battery was low enough
 */
public class FTP11Key {
	public native int GetBatteryStatus(int[] status);
}
