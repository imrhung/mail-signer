package com.tomica.mailsigner;


import java.io.File;
import java.io.FilenameFilter;
import com.entersafe.safeca.P11Other;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

/**
 * Model class for Main activity
 * @author TOMICALAB
 *
 */
public class MainModel {

	public Context context;
	public Handler handler;
	protected boolean btryConnect;
	protected boolean isConnected;
	protected boolean isLogin;

	public MainModel(Context ctx, Handler hdl) {
		context = ctx;
		handler = hdl;
	}

	/**
	 * @return path of first pdf file founded in External Storage.
	 */
	public String getPdfPath() {
		File images = Environment.getExternalStorageDirectory();
		File[] pdffile = images.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return ((name.endsWith(".pdf")));
			}
		});
		String path = pdffile[1].getAbsolutePath();

		Log.v("Path", path);
		return path;
	}

	/**
	 * To test connecting token. Not in use.
	 * @return
	 */
	public int connectToken() {
		if (P11Other.p11initialize()) {
			Log.v("Token", "Load success");
		} else {
			Log.v("Token", "Load fail");
		}
		return 0;
	}

	/**
	 * Method to test any things. :)
	 */
	public void test() {
		P11Other.p11initialize();
	}

	
}
