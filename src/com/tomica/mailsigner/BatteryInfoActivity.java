package com.tomica.mailsigner;

import iaik.pkcs.pkcs11.wrapper.PKCS11Exception;

import com.entersafe.safeca.Constant;
import com.entersafe.safeca.P11Other;
import com.entersafe.utils.FTP11Key;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.Resources;
import android.util.Log;
import android.widget.TextView;

/**
 * Activity to display batery information.
 * Not used yet.
 * @author TOMICALAB
 *
 */
public class BatteryInfoActivity extends Activity {

	private boolean isConnected;
	private TextView tvBattery;
	private Resources res;
	private int[] stat = new int[1];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(Constant.Debug, "onCreate Battery Info");
		setContentView(R.layout.activity_battery_info);
		res=this.getResources();
		tvBattery = (TextView)findViewById(R.id.tvStatusBattery);
		try {
			if (!P11Other.p11initialize()) {
				P11Other.p11finalize();
				isConnected = false;
			} else {
				isConnected = true;
				Log.e(Constant.Debug, "Connected");
			}
			FTP11Key pkey = new FTP11Key();
			pkey.GetBatteryStatus(stat);
			
			Log.e(Constant.Debug, "battery= "+stat[0]);
			
			if (isConnected)
			{
				P11Other.getInfo();
				P11Other.getSlotInfo();
				P11Other.getTokenInfo();
				P11Other.openSession();
				Log.e(Constant.Debug, "Get Info OK");
			}
		} catch (PKCS11Exception e) {
			e.printStackTrace();
			isConnected = false;
		}
		if(stat[0] == 1)
		{
			tvBattery.setText(res.getString(R.string.tv_battery_info_full));
		}
		else
		{
			tvBattery.setText(res.getString(R.string.tv_battery_info_weak));
		}
		
		
	}
	@Override
	protected void onResume() {
		Log.e(Constant.Debug, "onResume BatterInfo");
		super.onResume();
		
		try {
			if (!P11Other.p11initialize()) {
				P11Other.p11finalize();
				isConnected = false;
			} else {
				isConnected = true;
				Log.e(Constant.Debug, "Connected");
			}
			FTP11Key pkey = new FTP11Key();
			pkey.GetBatteryStatus(stat);
			
			Log.e(Constant.Debug, "battery= "+stat[0]);
			
			if (isConnected)
			{
				P11Other.getInfo();
				P11Other.getSlotInfo();
				P11Other.getTokenInfo();
				P11Other.openSession();
				Log.e(Constant.Debug, "Get Info OK");
			}
		} catch (PKCS11Exception e) {
			e.printStackTrace();
			isConnected = false;
		}
		if(stat[0] == 1)
		{
			tvBattery.setText(res.getString(R.string.tv_battery_info_full));
		}
		else
		{
			tvBattery.setText(res.getString(R.string.tv_battery_info_weak));
		}
	}

}
