/**
 * 
 */
package com.tomica.mailsigner.common;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.Wsdl2Code.WebServices.SignService.IWsdl2CodeEvents;
import com.entersafe.safeca.Constant;

/**
 * Main class to handle when interacting with web service
 * @author TOMICALAB
 *
 */
public class MailSignerEvents implements IWsdl2CodeEvents {
	
	// Define service name:
	public static final String M_UPLOAD_OFFICE_FILE = "UploadOfficeFile";
	public static final String M_GET_SIGNED_OFFICE_FILE = "GetSignedOfficeFile";
	public static final String M_UPLOAD_PDF_FILE = "UploadPdfFile";
	public static final String M_GET_SIGNED_PDF_FILE = "GetSignedPdfFile";
	public static final String M_BUILD_MAIL = "BuildMail";
	public static final String M_SEND_MAIL = "sendMail";
	public static final String M_SEND_MAIL_EX = "sendMailEx";
	
	public Object response;
	public Handler handler;
	
	public MailSignerEvents(Handler handler){
		response = null;
		this.handler =  handler;
	}

	/* 
	 * Before sending request to web service
	 */
	@Override
	public void Wsdl2CodeStartedRequest() {
		Log.d("Wsdl2Code", "Wsdl2CodeStartedRequest");
	}
	
	/* 
	 * Get response back
	 */
	@Override
	public void Wsdl2CodeFinished(String methodName, Object data) {
		if (data!= null){
			Log.d("Wsdl2CodeFin",data.toString());
		}
		if (methodName.equals(M_BUILD_MAIL)){
			// Build mail completed. Now let encrypt mail.
			Log.d("BuildMail", "Completed");
			// Send message back. 
			Message msg = new Message();
            msg.what = Constant.BUILD_MAIL_OK;
            msg.obj  = data;
            handler.sendMessage(msg);
			
		} else if (methodName.equals(M_SEND_MAIL_EX)){
			//Send mail with authentication completed
			Message msg = new Message();
            msg.what = Constant.SEND_MAIL_EX_OK;
            msg.obj  = data;
            handler.sendMessage(msg);
			
		} else if (methodName.equals(M_SEND_MAIL)){
			Message msg = new Message();
            msg.what = Constant.SEND_MAIL_OK;
            msg.obj  = data;
            handler.sendMessage(msg);
			
		} else if (methodName.equals(M_UPLOAD_OFFICE_FILE)){
			Message msg = new Message();
            msg.what = Constant.UP_OFFICE_OK;
            msg.obj  = data;
            handler.sendMessage(msg);
			
		} else if (methodName.equals(M_GET_SIGNED_OFFICE_FILE)){
			Message msg = new Message();
            msg.what = Constant.GET_SIGNED_OFFICE_OK;
            msg.obj  = data;
            handler.sendMessage(msg);
			
		} else if (methodName.equals(M_UPLOAD_PDF_FILE)){
			Message msg = new Message();
            msg.what = Constant.UP_PDF_OK;
            msg.obj  = data;
            handler.sendMessage(msg);
			
		} else if (methodName.equals(M_GET_SIGNED_PDF_FILE)){
			Message msg = new Message();
            msg.what = Constant.GET_SIGNED_PDF_OK;
            msg.obj  = data;
            handler.sendMessage(msg);
			
		} else {
			// Error or strange method name: System error.
		}
	}

	/* 
	 * If exception occur
	 */
	@Override
	public void Wsdl2CodeFinishedWithException(Exception ex) {
		Log.d("Wsdl2Code", "Wsdl2CodeFinishedWithException");
		Message msg = new Message();
        msg.what = Constant.SERVICE_EXCEPTION;
        handler.sendMessage(msg);
	}

	/* 
	 * Finish request
	 */
	@Override
	public void Wsdl2CodeEndedRequest() {
		Log.d("Wsdl2Code", "Wsdl2CodeEndedRequest");
	}

}
