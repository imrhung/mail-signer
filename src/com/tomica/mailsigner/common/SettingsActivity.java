package com.tomica.mailsigner.common;


import com.tomica.mailsigner.R;

import android.os.Bundle;
import android.preference.PreferenceActivity;


/**
 * Setting for user.
 * @author TOMICALAB
 *
 */
public class SettingsActivity extends PreferenceActivity {
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	   super.onCreate(savedInstanceState);
	   addPreferencesFromResource(R.xml.pref_general);
	}
}
