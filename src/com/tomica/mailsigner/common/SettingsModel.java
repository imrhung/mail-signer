package com.tomica.mailsigner.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingsModel {
	
	public static final String PREFS_ACCOUNT = "Account";
	public static final String EMAIL_ACCOUNT = "email";
	public static final String EMAIL_PASS = "password";
	public static final String SMTP_SERVER = "smtp_server";
	public static final String SMTP_PORT = "smtp_port";
	
	public Context context;
	
	public SettingsModel(Context ctx){
		context =ctx;
	}

	public void saveAccount(String email, String pass, String smtp, String port) {
		
		SharedPreferences settings = context.getSharedPreferences(PREFS_ACCOUNT, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(EMAIL_ACCOUNT, email);
		editor.putString(EMAIL_PASS, pass);
		editor.putString(SMTP_SERVER, smtp);
		editor.putString(SMTP_PORT, port);

		// Commit the edits!
		editor.commit();
	}

	public String getEmailAccount() {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_ACCOUNT, 0);
	    String email = prefs.getString(EMAIL_ACCOUNT, null);
		return email;
	}
	
	public String getEmailPass() {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_ACCOUNT, 0);
	    String pass = prefs.getString(EMAIL_PASS, null);
		return pass;
	}
	
	public String getSmtpServer() {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_ACCOUNT, 0);
	    String smtp = prefs.getString(SMTP_SERVER, null);
		return smtp;
	}
	
	public int getSmtpPort() {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_ACCOUNT, 0);
	    String str = prefs.getString(SMTP_PORT, "25");
	    int port = Integer.valueOf(str);
		return port;
	}

}
