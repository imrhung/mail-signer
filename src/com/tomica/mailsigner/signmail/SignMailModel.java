/**
 * 
 */
package com.tomica.mailsigner.signmail;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.Wsdl2Code.WebServices.SignService.SignService;
import com.Wsdl2Code.WebServices.SignService.VectorByte;
import com.Wsdl2Code.WebServices.SignService.VectorString;
import com.entersafe.safeca.Constant;
import com.tomica.mailsigner.common.MailSignerEvents;
import com.tomica.mailsigner.common.SettingsModel;
import com.tomica.mailsigner.token.Token;

/**
 * This model class handle all action for this EmailSign Activity.
 * @author TOMICALAB
 */
public class SignMailModel{
	private static SignMailModel _signModel=null;
	static MailSignerEvents buildMail;
	Context context;
	SignService service;
	Handler handler;
	SettingsModel settings;
	Token token;

	/**
	 * Constructor
	 */
	
	public static SignMailModel  getInstance(Context ctx, Handler handler){
		if(_signModel==null){
			_signModel = new SignMailModel(ctx,handler);
		}
		return _signModel;	
	}
		
	public SignMailModel(Context ctx, Handler handler){
		context = ctx;
		buildMail = new MailSignerEvents(handler);
		service = new SignService(buildMail);
		settings = new SettingsModel(context);
		token = new Token(context, handler);
		this.handler =  handler;
	}

	public void startSending(String content){
		// First is build mail to Base64
		Log.v("SignMail", "Start sending");
		this.buildMail(content);
	}

	public void buildMail(String content){
		try {
			service.BuildMailAsync(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void signObjectVectorByte(byte[] data, String pin) {
		String signedData = token.signDataBase64(data, pin);
//		vectorData = new VectorByte(signedData);
		
		Message msg = new Message();
        msg.what = Constant.MAIL_SIGNED_PKCS_OK;
        msg.obj  = signedData;
        handler.sendMessage(msg);
	}

	public void sendMailEx(String sigPkcs1Base64,VectorString listMailTo,VectorString listCC,String mailSubject, String pin, String randomString){
		String certbase64;
		String smtpServer;
		int port;
		String username;
		String password;
		String mailFrom;
		
		certbase64 = getCertBase64(pin);
		smtpServer = getSmtpServer();
		port = getPort();
		username = getEmailAccount();
		password = getEmailPass();
		mailFrom = getEmailAccount();
		try {
			service.sendMailExAsync(sigPkcs1Base64, certbase64, smtpServer, port, username, password, 
					mailFrom, listMailTo, listCC, mailSubject, randomString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getCertBase64(String pin){
		return token.getCertBase64(pin);
	}
	
	public String getSmtpServer(){
		// TODO
		return settings.getSmtpServer();
	}
	public int getPort(){
		// TODO
		return settings.getSmtpPort();
	}
	
	
	public String getEmailAccount() {
		return settings.getEmailAccount();
	}

	public String getEmailPass() {
		return settings.getEmailPass();
	}


}
