package com.tomica.mailsigner;

import android.content.Context;
import android.os.Handler;


/**
 * Model file for ChangePinActivity
 * @author TOMICALAB
 *
 */
public class ChangePinModel {
	
	public Context context;
	public Handler handler;
	
	public ChangePinModel(Context ctx, Handler hdl){
		context = ctx;
		handler = hdl;
	}

}
