package com.tomica.mailsigner;

import cfca.mobile.keydevice.AudioToken;

import com.entersafe.safeca.Constant;
import com.entersafe.safeca.MyProgressDialog;
import com.entersafe.safeca.P11Other;
import com.tomica.mailsigner.token.Token;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

/**
 * Activity to change user PIN.
 * Not in use.
 * @author TOMICALAB
 *
 */
public class ChangePinActivity extends Activity {

	private ImageButton btn_changepin_ok;
	private EditText et_changepin_oldpin;
	private EditText et_changepin_newpin;
	private EditText et_changepin_confirmpin;
	private Context context;
	private Resources res;
	private ChangePinModel mModel;

	private Handler handler;
	private Token token;

	boolean btryConnect = false;
	public boolean isLogin = false;
	protected long[] keyHandle = null;
	protected long[] keyHandle1024 = null;
	protected long[] keyHandle2048 = null;

	public MyProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_pin);
		context = this;
		res = this.getResources();
		handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case Constant.SHOWPROGRESSDIALOG:
					Log.e(Constant.Debug, "SHOWPROGRESSDIALOG");
					progressDialog.setShowInfo(res
							.getString(R.string.inform_tryconnect_init));
					progressDialog.show();
					break;
				case Constant.CLOSEPROGRESSDIALOG:
					progressDialog.dismiss();
					break;
				case Constant.TOKEN_CONNECT_OK:

					break;
				case Constant.SHOWERRINFO:
					if (null != msg.obj) {
						if (msg.obj
								.toString()
								.compareTo(
										res.getString(R.string.inform_handset_connectfail)) == 0) {
							String title, button;
							button = res.getString(R.string.inform_button_OK);
							title = res.getString(R.string.inform_title);
							new Builder(context)
									.setTitle(title)
									.setMessage(msg.obj.toString())
									.setPositiveButton(
											button,
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(
														DialogInterface arg0,
														int arg1) {
													// TODO: error handle.
													// finish();
												}
											}).show();
						} else
							showMessage(msg.obj.toString());
					}
					break;
				case 0:

					break;
				default:
					super.handleMessage(msg);
				}
			}
		};
		
		token = new Token(context, handler);
		mModel = new ChangePinModel(context, handler);
		progressDialog = new MyProgressDialog(this);
		P11Other.audioToken = new AudioToken(this);

		btn_changepin_ok = (ImageButton) findViewById(R.id.btn_changepin_ok);

		et_changepin_oldpin = (EditText) findViewById(R.id.et_oldpin);
		et_changepin_newpin = (EditText) findViewById(R.id.et_newpin);
		et_changepin_confirmpin = (EditText) findViewById(R.id.et_confirmpin);

		btn_changepin_ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String oldpin = et_changepin_oldpin.getText().toString();
				String newpin = et_changepin_newpin.getText().toString();
				String confirmpin = et_changepin_confirmpin.getText()
						.toString();
				if (oldpin.compareTo("") == 0 || newpin.compareTo("") == 0
						|| confirmpin.compareTo("") == 0) {
					showDialog(0);
				} else {
					if (newpin.compareTo(confirmpin) != 0) {
						et_changepin_newpin.setText("");
						et_changepin_confirmpin.setText("");
						showDialog(1);
					} else {
						et_changepin_oldpin.setText("");
						et_changepin_newpin.setText("");
						et_changepin_confirmpin.setText("");

						// TODO : Handle here
						progressDialog.setShowInfo(res
								.getString(R.string.inform_changepin_wait));
						token.sleep(0, Constant.SHOWPROGRESSDIALOG, "");
						token.sleep(100, Constant.CHANGEPASSWORD, oldpin,
								newpin);
					}
				}

			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			return new AlertDialog.Builder(context)
					.setTitle(res.getString(R.string.inform_title))
					.setMessage(res.getString(R.string.inform_input_notnull))
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
								}
							}).create();
		case 1:
			return new AlertDialog.Builder(context)
					.setTitle(res.getString(R.string.inform_title))
					.setMessage(
							res.getString(R.string.inform_input_newandconfirm))
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
								}
							}).create();
		default:
			break;
		}
		return super.onCreateDialog(id);
	}

	private void showMessage(String msg) {
		if (null == msg || "" == msg)
			return;

		String title, button;
		button = res.getString(R.string.inform_button_OK);
		title = res.getString(R.string.inform_title);
		new Builder(this).setTitle(title).setMessage(msg)
				.setPositiveButton(button, null).show();
	}

}
