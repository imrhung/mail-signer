package com.tomica.mailsigner.fragment;

import com.entersafe.safeca.Constant;
import com.tomica.mailsigner.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class PasswordDialogFragment extends DialogFragment implements OnEditorActionListener {

	public interface PasswordDialogListener {
		void onFinishEditDialog(String inputText);
	}

	private EditText mEditText;

	public PasswordDialogFragment() {
		// Empty constructor required for DialogFragment
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		// Get the layout inflater
		View promptView = inflater.inflate(R.layout.dialog_password, null);

		mEditText = (EditText) promptView.findViewById(R.id.edtInputPass);

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		builder.setView(promptView)
		// Add action buttons
		.setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				// Continue signing
				PasswordDialogListener activity = (PasswordDialogListener) getActivity();
				activity.onFinishEditDialog(mEditText.getText().toString());
			}
		})
		.setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// Do nothing.
			}
		});

		return promptView;
	}
	

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {
			// Return input text to activity
			PasswordDialogListener activity = (PasswordDialogListener) getActivity();
			activity.onFinishEditDialog(mEditText.getText().toString());
			this.dismiss();
			return true;
		}
		return false;
	}
}