package com.tomica.mailsigner.fragment;

import com.tomica.mailsigner.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;

public class PassDialogFragment extends DialogFragment {
	
    private EditText mEditText;
    
    public interface PasswordDialogListener {
		void onFinishEditDialog(String inputText);
	}

    public static PassDialogFragment newInstance() {
    	PassDialogFragment frag = new PassDialogFragment();
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_password, null);

        mEditText = (EditText) view.findViewById(R.id.edtInputPass);

        alertDialogBuilder.setView(view);
        alertDialogBuilder.setTitle(getString(R.string.txt_dialog_title_password));
        alertDialogBuilder.setPositiveButton(getString(R.string.txt_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	// Continue signing
            	Intent intent = new Intent();
            	intent.putExtra("pass", mEditText.getText().toString());
				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        return alertDialogBuilder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}