package com.tomica.mailsigner.fragment;

import com.tomica.mailsigner.R;
import com.tomica.mailsigner.common.BatteryInfoActivity;
import com.tomica.mailsigner.common.ChangePinActivity;
import com.tomica.mailsigner.common.SettingsActivity;
import com.tomica.mailsigner.common.SettingsModel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class SettingsFragment extends Fragment implements OnClickListener{
	
	private static final String TAG = "SettingsFragment";
	private EditText edtEmail;
	private EditText edtPass;
	private EditText edtSmtp;
	private EditText edtPort;
	
	private Button btnCert;
	private Button btnChangePin;
	private Button btnBattery;
	private Button btnHelp;
	
	private SettingsModel settings;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settings = new SettingsModel(getActivity());
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
    	View rootView =  inflater.inflate(R.layout.fragment_settings, container, false);
    	

        edtEmail = (EditText) rootView.findViewById(R.id.edtEmailSettings);
    	edtPass = (EditText) rootView.findViewById(R.id.edtPassSettings);
    	edtSmtp = (EditText) rootView.findViewById(R.id.edtSmtpSettings);
    	edtPort = (EditText) rootView.findViewById(R.id.edtPortSettings);
    	
    	btnCert = (Button) rootView.findViewById(R.id.btnCertSettings);
    	btnChangePin = (Button) rootView.findViewById(R.id.btnChangePinSettings);
    	btnBattery = (Button) rootView.findViewById(R.id.btnBatterySettings);
    	btnHelp = (Button) rootView.findViewById(R.id.btnHelpSettings);
    	
    	return rootView;
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    	Log.v(TAG, "Resume settings");
    	edtEmail.setText(settings.getEmailAccount());
    	edtPass.setText(settings.getEmailPass());
    	edtSmtp.setText(settings.getSmtpServer());
    	edtPort.setText(Integer.toString(settings.getSmtpPort()));
    }
    
    @Override
    public void onPause(){
    	super.onPause();
    	Log.v(TAG, "Save settings");
    	settings.saveAccount(
    			edtEmail.getText().toString(), 
    			edtPass.getText().toString(), 
    			edtSmtp.getText().toString(), 
    			edtPort.getText().toString());
    }

    @Override
    public void onClick(View v) {
    	switch (v.getId()){
    	case R.id.btnCertSettings:
    		
    		break;
    	case R.id.btnChangePinSettings:
    		Intent intent = new Intent(getActivity(), ChangePinActivity.class);
			startActivity(intent);
    		break;
    	case R.id.btnBatterySettings:
    		Intent battery = new Intent(getActivity(), BatteryInfoActivity.class);
			startActivity(battery);
    		break;
    	case R.id.btnHelpSettings:
    		
    		break;
    	default:
    		break;
    	}

    }
}