package com.tomica.mailsigner;

import com.tomica.mailsigner.fragment.SettingsFragment;
import com.tomica.mailsigner.fragment.SignMailFragment;
import com.tomica.mailsigner.fragment.SignPdfFragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class AppFragmentPageAdapter extends FragmentPagerAdapter{
	
	private Context context;
	
	public AppFragmentPageAdapter(Context ctx, FragmentManager fm) {
        super(fm);
        context = ctx;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                return new SignMailFragment();
            case 1:
                return new SignPdfFragment();
            case 2:
                return new SettingsFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
    	switch (position) {
    	case 0:
    		return context.getString(R.string.tab_sign_mail);
    	case 1:
    		return context.getString(R.string.tab_sign_pdf);
    	case 2:
    		return context.getString(R.string.tab_settings);
    	default:
    		return null;
    	}
    }
}
